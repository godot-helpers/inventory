extends Sprite

###############################################################################
#
# DEMO HELPERS
#
# The below are not needed in your own implementation, and are there only
# for the purposes of the demonstration
# 

# records if the mouse is pressed, or if a finger is pressed
var is_pressed = false
# is true for one frame, when `is_pressed` switches
var has_just_changed = true
# 
var last_event_position: Vector2 = Vector2.ZERO

# We're using a signal callback on a Control node here, so we can track
# when the mouse trails off, and so Controls on top can prevent the mouse
# from working below. But any process that captures input events would work
# We only want to record the pressed status, and the event position
# Using a control also allows us to get mouse_exited (without it, the cursor)
# would still get recorded if you drag out of the game window
func _on_gui_input(event: InputEvent):
	if InputProxy.is_tap_event(event):
		var was_pressed = is_pressed
		match control_mode:
			CONTROL_MODE.VIRTUAL_JOYSTICK:
				# If we're using the virtual joystick, we check if the mouse is within
				# boundaries. We could also simply use a Control node, that'd work too
				is_pressed = InputProxy.sprite_is_pressed(event, self)
			_:
				# For all other methods, it's enough to set `is_pressed` to the event's
				# `pressed` property
				is_pressed = event.pressed
		has_just_changed = (was_pressed != is_pressed)
	
	if InputProxy.is_tap_event(event) or InputProxy.is_move_event(event):
		last_event_position = event.position

# Called from the Control's signal. When the mouse exits the play area,
# we want to cancel inputs
func _on_mouse_exit():
	if is_pressed:
		is_pressed = false
		has_just_changed = true

# processes the position on each frame and updates
# the joystick cursor if need be
func _process(_delta):

	var vec: Vector2 = Vector2.ZERO

	if not is_pressed:
		if has_just_changed:
			has_just_changed = false
			clear_debug_line()
			_update_position(vec)
		return

	# This is for the demo; realistically, you'd probably want to stick with one method
	match control_mode:
		CONTROL_MODE.VIRTUAL_JOYSTICK:
			vec = InputProxy.get_unit_vector(get_local_mouse_position(), texture.get_size(), centered, offset)
			update_debug_line(character.global_position + vec * 32, character.global_position)
		CONTROL_MODE.SCREEN_JOYSTICK:
			update_debug_line(last_event_position, area.rect_size / 2)
			vec = InputProxy.get_unit_vector(last_event_position, area.rect_size, false)
		CONTROL_MODE.SCREEN_TAP:
			update_debug_line(last_event_position, character.global_position)
			vec = InputProxy.get_unit_vector_non_centered(last_event_position, character.global_position, area.rect_size)
	
	_update_position(vec)

# Updates both the joystick cursor to match the direction, and dispatches
# actions (acts as if the controller sent the actions)
func _update_position(vec: Vector2):
	joystick_cursor.position = InputProxy.unit_vector_to_location(vec, self)
	InputProxy.direction_to_action(vec, action_names)

###############################################################################
#
# DEMO HELPERS
#
# The below are not needed in your own implementation, and are there only
# for the purposes of the demonstration
# 

enum CONTROL_MODE{
	NONE,
	VIRTUAL_JOYSTICK,
	SCREEN_JOYSTICK,
	SCREEN_TAP
}

export(PoolStringArray) var action_names = ['ui_up','ui_right','ui_down','ui_left']

export(CONTROL_MODE) var control_mode :int = CONTROL_MODE.NONE

export(NodePath) var character_node_path: NodePath = "../Character"
onready var character: KinematicBody2D = get_node(character_node_path)

export(NodePath) var mode_selector_path: NodePath = "../OptionButton"
onready var optionButton: OptionButton = get_node(mode_selector_path)

onready var area: Control = $Area
onready var joystick_cursor: Sprite = $Cursor

func _ready():
	# Add the modes to the select box so they can be tested
	# each on their own
	for mode in CONTROL_MODE:
		var idx = CONTROL_MODE[mode]
		var mode_name = mode.replace("_", " ").to_lower()
		optionButton.add_item(mode_name, idx)
	optionButton.select(control_mode)
	# Wire the select box
	assert(OK == optionButton.connect("item_selected", self, "_on_OptionButton_item_selected"))
	# Wire the Control to capture mouse movements
	assert(OK == area.connect("gui_input", self, "_on_gui_input"))
	assert(OK == area.connect("mouse_exited", self, "_on_mouse_exit"))

func _on_OptionButton_item_selected(idx: int) -> void:
	control_mode = idx

onready var line:Line2D = $"../Line2D"
func update_debug_line(p1:Vector2, p2:Vector2) -> void:
	line.clear_points()
	line.add_point(p1)
	line.add_point(p2)

func clear_debug_line() -> void:
	line.clear_points()