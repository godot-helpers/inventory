
extends KinematicBody2D

var speed = 200  # speed in pixels/sec

var velocity = Vector2.ZERO

onready var virtual_joystick = $"../VirtualJoystick"

func _physics_process(_delta):
	if not is_instance_valid(virtual_joystick):
		return
	velocity = InputProxy.actions_to_vector(virtual_joystick.action_names) * speed
	velocity = move_and_slide(velocity)

