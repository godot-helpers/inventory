extends Sprite

var is_pressed = false
enum CONTROL_MODE{
	VIRTUAL_JOYSTICK,
	SCREEN_JOYSTICK,
	SCREEN_TAP
} 
export(CONTROL_MODE) var control_mode := CONTROL_MODE.SCREEN_TAP
export(NodePath) var character_node_path = "../Character"
onready var character = get_node(character_node_path)

func _input(event: InputEvent):
	match control_mode:
		CONTROL_MODE.VIRTUAL_JOYSTICK:
			if is_tap_event(event):
				is_pressed = sprite_is_pressed(event, self)
				if not is_pressed:
					_update_position(Vector2.ZERO)
			elif is_move_event(event) and is_pressed:
				_update_position(process_sprite_cursor(self))
		CONTROL_MODE.SCREEN_JOYSTICK:
			if is_tap_event(event):
				_update_position(process_screen_cursor(event, self))
		CONTROL_MODE.SCREEN_TAP:
			if is_tap_event(event):
				is_pressed = event.pressed
			if is_pressed:
				_update_position(process_screen_tap_event(event, character))
			else:
				_update_position(Vector2.ZERO)

func _update_position(vec: Vector2):
	$cursor.position = direction_to_position(vec, self)
	direction_to_input(vec, ['ui_up','ui_right','ui_down','ui_left'])

static func process_screen_tap_event(event:InputEvent, character_node:Node2D) -> Vector2:
	var cursor_position = event.position
	var character_position = character_node.global_position
	var area_size = character_node.get_viewport_rect().size
	var vec = process_non_centered_cursor(cursor_position, character_position, area_size)
	return vec

static func is_tap_event(event: InputEvent):
	return event is InputEventMouseButton or event is InputEventScreenTouch

static func is_move_event(event: InputEvent):
	return event is InputEventMouseMotion or event is InputEventScreenDrag

static func direction_to_position(vec: Vector2, sprite: Sprite) -> Vector2:
	return vec * sprite_get_bounds(sprite)

static func direction_to_input(vec: Vector2, directions: PoolStringArray = ['up','right','down','left']):
	if vec == null or vec == Vector2.ZERO:
		for key in directions:
			Input.action_release(key)
		return

	if vec.x > 0:
		Input.action_press(directions[1],vec.x)
	elif vec.x < 0:
		Input.action_press(directions[3], - vec.x)
	else:
		Input.action_release(directions[1])
		Input.action_release(directions[3])
	if vec.y > 0:
		Input.action_press(directions[2], vec.y)
	elif vec.y < 0:
		Input.action_press(directions[0], - vec.y)
	else:
		Input.action_release(directions[2])
		Input.action_release(directions[0])

static func sprite_is_pressed(event: InputEvent, sprite: Sprite) -> bool:
	return \
		'pressed' in event and \
		event.pressed == true and \
		cursor_is_over_sprite(event.position, sprite)

static func sprite_get_bounds(sprite: Sprite) -> Vector2:
	return - sprite.offset + ((sprite.texture.get_size() / 2.0) if sprite.centered else Vector2.ZERO )

static func get_sprite_rect(sprite: Sprite) -> Rect2:
	var size = sprite.texture.get_size()
	var pos = sprite.global_position + sprite.offset - ((size / 2.0) if sprite.centered else Vector2.ZERO )
	var rect = Rect2(pos, size * sprite.scale)
	return rect

static func cursor_is_over_sprite(cursor_position: Vector2, sprite: Sprite) -> bool:
	var rect = get_sprite_rect(sprite)
	return rect.has_point(cursor_position)

static func process_sprite_cursor(sprite: Sprite) -> Vector2:
	return process_cursor(sprite.get_local_mouse_position(), sprite.texture.get_size(), sprite.centered, sprite.offset)

static func process_screen_cursor(event: InputEvent, sprite: Sprite) -> Vector2:
	return process_cursor(event.position, sprite.get_viewport_rect().size, false) if event.pressed else Vector2.ZERO
	
static func process_cursor(cursor_position:Vector2, area_size: Vector2, origin_is_center:bool = true, offset: Vector2 = Vector2.ZERO) -> Vector2:
	var character_pos = cursor_position + offset - (Vector2.ZERO if origin_is_center else area_size/2.0)
	var position = ((character_pos / area_size) * 2)
	var vec = Vector2(clamp(position.x, -1, 1), clamp(position.y, -1, 1))
	return vec

static func process_non_centered_cursor(cursor_position: Vector2, character_position: Vector2, area_size: Vector2):
	var pane = area_size - character_position
	var position = (cursor_position - character_position) / pane
	var vec = Vector2(clamp(position.x, -1, 1), clamp(position.y, -1, 1))
	return vec
