extends Sprite

var entity: GridMovementEntity

func _ready():
	# registering the current tilemap. Normally, you'd probably do that when you
	# load a level
	GridMovementManager.tileMap = $"../TileMap"
	# registering the entity
	entity = GridMovementManager.register(self)

func _physics_process(_delta):
	var speed = InputProxy.actions_to_vector(['ui_up','ui_right','ui_down','ui_left'])
	entity.direction = InputProxy.discretize_unit_vector(speed)
	entity.speed_modifier = get_speed(speed, "ui_accept", "ui_select", SPEED_ACQUIRE_MODE.DISCREET)

###############################################################################
#
# DEMO-RELATED STUFF
#
# the below determines the speed of the sprite
# There are two methods, keyboard and/or virtual controllers/joypads, etc
#

# The lower the value, the faster the character will go
const STEP_TIME = {
	"NONE": 0,
	"NORMAL": 1.0,
	"SNEAK": 4.0,
	"RUN": 0.5
}

enum SPEED_ACQUIRE_MODE{
	DISCREET,
	ANALOG
}

static func get_speed(speed: Vector2, action_sneak: String, action_run: String, mode: int = SPEED_ACQUIRE_MODE.DISCREET) -> float:
	if not speed or speed == Vector2.ZERO:
		return 0.0
	if mode == SPEED_ACQUIRE_MODE.ANALOG:
		return get_speed_from_input_strength(speed)
	else:
		return get_speed_from_modifiers(action_sneak, action_run)

static func get_speed_from_modifiers(action_sneak: String, action_run: String) -> float:
	if Input.is_action_pressed(action_sneak):
		return STEP_TIME.SNEAK
	elif Input.is_action_pressed(action_run):
		return STEP_TIME.RUN
	else:
		return STEP_TIME.NORMAL
static func get_speed_from_input_strength(speed: Vector2) -> float:
	var pace = speed.length()

	if pace < 0.1:   return STEP_TIME.NONE
	elif pace < 0.3: return STEP_TIME.SNEAK
	elif pace > 0.9: return STEP_TIME.RUN
	else:            return STEP_TIME.NORMAL
