extends Node2D

var demos = {
	"choose a demo": null,
	"inventory": preload('./inventory/inventory.tscn').instance(),
	"grid": preload('./grid/grid.tscn').instance(),
	"joystick": preload('./joystick/joystick.tscn').instance()
}
var screens = []

onready var optionButton: OptionButton = $HBoxContainer/OptionButton
onready var backButton: Button = $HBoxContainer/BackButton

func _ready():
	var demos_keys = demos.keys()
	for idx in range(demos_keys.size()):
		var screen_name = demos_keys[idx]
		var screen = demos[screen_name]
		optionButton.add_item(screen_name, idx)
		optionButton.set_item_metadata(idx, screen)
	assert(OK == optionButton.connect("item_selected", self, "_on_item_selected"))
	assert(OK == backButton.connect("pressed", self, "_on_backButton_pressed"))
	_update_back_button_text()
	load_initial_scene_if_any()

func load_initial_scene_if_any():
	var initial_scene = SceneSelector.load_scene(self)
	if initial_scene: push_screen(initial_scene)

func _update_back_button_text():
	backButton.text = '<' if screens.size() > 0 else 'X'

func push_screen(screen:Node2D):
	if screens.size():
		var previous_screen = screens.back()
		$Screen.remove_child(previous_screen)
	screens.push_back(screen)
	$Screen.add_child(screen)
	_update_back_button_text()

func pop_screen() -> bool:
	var screen = screens.pop_back()
	if screen != null:
		$Screen.remove_child(screen)
		if screens.size():
			var previous_screen = screens.back()
			$Screen.add_child(previous_screen)
		_update_back_button_text()
		return true
	return false

func _on_item_selected(idx: int):
	var screen = optionButton.get_item_metadata(idx)
	if screen != null:
		push_screen(screen)

func _back_or_quit():
	if pop_screen():
		return
	else:
		get_tree().quit()

func _on_backButton_pressed():
	_back_or_quit()

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST or what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST:
		_back_or_quit()
