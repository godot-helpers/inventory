extends Node2D


################################################################################
# 
# RECIPES
# 

var book: InventoryRecipeBook

func _on_init_load_recipes():
	book = Inventory.get_all_recipes_in_directory("res://samples/inventory/recipes")

################################################################################
# 
# BUTTON WINDOWS MANAGEMENT
#
# the scene contains a few buttons that pop-up windows. Those buttons have the
# windows as children.
#

var windows = {}

# Runs on _ready()
func _on_ready_prepare_buttons():
	var buttons = [
		$InventoryButton,
		$NPC,
		$Equipment
	]
	for button in buttons:
		# This adds a window that is a child of a button to the inventory manager, and 
		# connects two methods that trigger when the window opens or closes.
		# We pass the button itself so we can toggle it on or off depending on the
		# window state
		# Finally, we index the window by button, so we can find it back
		# when the button is pressed
		var window = button.get_node("Window")
		window.window_title = button.text
		window.connect("is_opening", self, "_on_window_opening", [button])
		window.connect("is_closing", self, "_on_window_closing", [button])
		window.connect("item_drag_started", self, "_on_item_drag_started")
		window.connect("item_drag_forbidden", self, "_on_item_drag_forbidden")
		window.connect("item_set", self, "_on_item_set", [window])
		window.connect("item_removed", self, "_on_item_removed", [window])
		# Using Inventory.add_window removes the window from its current parent
		# So we index it in this `windows` object so we can find it back later 
		windows[button] = Inventory.add_window(window)
		# warning-ignore:return_value_discarded
		button.connect("pressed", self, "_on_button_pressed", [button])

# This triggers when a button containing a window is pressed
func _on_button_pressed(button):
	# warning-ignore:return_value_discarded
	var window = windows[button]
	Inventory.toggle_window(window, true)

# This triggers when a window opens
func _on_window_opening(button:Button) -> void:
	button.pressed = true

# This triggers when a window closes
func _on_window_closing(button:Button) -> void:
	button.pressed = false

# This refreshes the available crafting options when an item is added
func _on_item_set(_item: InventoryItem, window: WindowDialog) -> void:
	if window.window_title == "Inventory":
		refresh_available_recipes(window, window.items)

# This refreshes the available crafting options when an item is removed
func _on_item_removed(_item: InventoryItem, window: WindowDialog) -> void:
	if window.window_title == "Inventory":
		refresh_available_recipes(window, window.items)

# This checks which recipes are available depending on what items the user holds
# Then it adds buttons to a list of buttons
func refresh_available_recipes(window:InventoryItemGridWindow, items: Array) -> void:
	var available = book.get_recipes_available(items)
	for child in $Recipes.get_children():
		$Recipes.remove_child(child)
		child.queue_free()
	for i in range(available.size()):
		var recipe: InventoryRecipe = available[i]
		var button = Button.new()
		button.text = recipe.result.name
		button.connect("pressed", self, "_on_recipe_click",[window, recipe])
		$Recipes.add_child(button)

func _on_recipe_click(window:InventoryItemGridWindow, recipe: InventoryRecipe) -> void:
	window.consume_recipe(recipe)

################################################################################
# 
# GENERAL WINDOW MANAGEMENT
#
# This handles dragging and dropping. Dragging and dropping _is_ handled 
# quasi-natively, but the decision of the drag icon is left to the user
#

# When a drag starts, set the current item as a drag preview
func _on_item_drag_started(item: InventoryItem):
	Inventory.set_drag_preview_item(item)

# If the drag is not allowed, for whatever reason (this is decided by setting
# the window's or the slot's `allow_drag` to `false`, then show
# an X mark under the mouse
func _on_item_drag_forbidden(_item: InventoryItem):
	Inventory.set_drag_preview_texture(preload('assets/forbidden.svg'))

################################################################################
# 
# BOOTSTRAPPING
# 

func _init():
	_on_init_load_recipes()

func _ready():
	_on_ready_prepare_buttons()
