extends Node
class_name SceneSelector

# Returns a string representing the current scene,
# or an empty string
static func get_scene_path() -> String:
	var constants = preload("./constants.gd")
	var SETTINGS_KEY = constants.SETTINGS_KEY
	var path = ProjectSettings.get_setting(SETTINGS_KEY)
	if path:
		return path
	return ""

# loads the selected scene. Accepts an optional `from`
# argument which checks that the loaded scene isn't itself
static func load_scene(from:Node = null) -> Node:
	var scene_path = get_scene_path()
	if scene_path:
		if from and from.filename == scene_path:
			return null
		var packed_scene: PackedScene = load(scene_path)
		var scene = packed_scene.instance()
		return scene
	return null