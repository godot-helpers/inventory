tool
extends EditorPlugin

const PLUGIN_CONFIG_DIR = 'addons'
const PLUGIN_CONFIG = 'settings.cfg'

var constants = preload("./constants.gd")
var PLUGIN_NAME = constants.PLUGIN_NAME
var SETTINGS_KEY = constants.SETTINGS_KEY

var scene_path: LineEdit = LineEdit.new()
var file_browser_button: ToolButton = ToolButton.new()
var run_button: ToolButton = ToolButton.new()
var box: HBoxContainer = HBoxContainer.new()
var file_dialog: FileDialog = FileDialog.new()
var plugin_config = ConfigFile.new()
var was_manually_set: bool = false
var main_scene_path: String = ProjectSettings.get_setting("application/run/main_scene")

func _enter_tree() -> void:
	
	box.add_child(scene_path)
	scene_path.rect_min_size = Vector2(250, 20)
	scene_path.text = get_current_scene_path()
	load_settings()
	scene_path.connect("text_changed", self, "on_scene_path_text_changed")

	file_browser_button.icon = get_editor_interface().get_base_control().get_icon("File", "EditorIcons")
	file_browser_button.connect("pressed", self, "_on_file_browser_button_pressed")
	box.add_child(file_browser_button)

	run_button.icon = get_editor_interface().get_base_control().get_icon("Play", "EditorIcons")
	run_button.connect("pressed", self, "_on_run_button_pressed")
	box.add_child(run_button)
	
	file_dialog.mode = FileDialog.MODE_OPEN_FILE
	file_dialog.access = FileDialog.ACCESS_RESOURCES
	file_dialog.set_filters(PoolStringArray(["*.tscn ; Godot Scenes"]))
	file_dialog.connect("file_selected", self, "_on_file_dialog_file_selected")
	get_editor_interface().get_base_control().add_child(file_dialog)

	connect("scene_changed", self, "_on_scene_changed")
	add_control_to_container(CONTAINER_TOOLBAR, box)

func _exit_tree() -> void:
	remove_control_from_container(CONTAINER_TOOLBAR, box)
	box.free()
	file_dialog.queue_free()

func _on_file_browser_button_pressed() -> void:
	file_dialog.popup_centered_ratio()

func _on_file_dialog_file_selected(path: String) -> void:
	if path:
		scene_path.text = path
		was_manually_set = true
		save_settings()
	else:
		scene_path.text = get_current_scene_path()
		was_manually_set = false

func _set_scene_from_current_unless_was_manually_set():
	if was_manually_set:
		return
	scene_path.text = get_current_scene_path()

func get_current_scene_path() -> String:
	var current_scene: Node = get_editor_interface().get_edited_scene_root()
	if current_scene:
		return current_scene.filename
	return ""

func on_scene_path_text_changed(new_text: String) -> void:
	if new_text == "":
		was_manually_set = false
		save_settings()

func get_config_path():
	var dir = get_editor_interface().get_editor_settings().get_project_settings_dir()
	var home = dir.plus_file(PLUGIN_CONFIG_DIR).plus_file(PLUGIN_NAME)
	var path = home.plus_file(PLUGIN_CONFIG)
	return path

func _on_scene_changed(new_scene: Node):
	if was_manually_set:
		return
	scene_path.text = new_scene.filename

func load_settings():
	var path = get_config_path()
	var fs = Directory.new()
	if not fs.file_exists(path):
		var config = ConfigFile.new()
		fs.make_dir_recursive(path.get_base_dir())
		config.save(path)
	else:
		plugin_config.load(path)
		var text = plugin_config.get_value("general","scene","")
		if text != "":
			scene_path.text = text
			was_manually_set = true

func save_settings():
	plugin_config.get_value("general", "scene", scene_path.text)
	plugin_config.save(get_config_path())

func _on_run_button_pressed() -> void:
	var current_scene = scene_path.text
	ProjectSettings.set_setting(SETTINGS_KEY, current_scene)
	get_editor_interface().play_main_scene()