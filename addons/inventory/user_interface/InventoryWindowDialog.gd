tool
extends WindowDialog
class_name InventoryWindowDialog, "InventoryWindowDialog.svg"

signal is_closing
signal is_opening

func _notification(what):
	if what == NOTIFICATION_VISIBILITY_CHANGED:
		if visible:
			emit_signal("is_opening")
		else:
			emit_signal("is_closing")

func center():
	rect_position = get_viewport().size / 2 - rect_size / 2

# sets an internal container
func _set_container(parent: Control, container: Control, margins:=0.0) -> Control:
	container.size_flags_horizontal = SIZE_EXPAND_FILL
	container.size_flags_vertical = SIZE_EXPAND_FILL
	container.mouse_filter = MOUSE_FILTER_PASS
	parent.add_child(container)
	#container.rect_size = parent.rect_size
	container.anchor_right = 1
	container.anchor_bottom = 1
	if margins > 0:
		container.margin_left = margins
		container.margin_top = margins
		container.margin_right = -margins
		container.margin_bottom = -margins
	return container

func shrink_size():
	if not is_inside_tree(): yield(self, "ready")
	yield(get_tree(),"idle_frame")
	rect_size = Vector2.ZERO