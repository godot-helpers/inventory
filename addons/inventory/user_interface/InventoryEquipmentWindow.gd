tool
extends InventoryWindowDialog
class_name InventoryEquipmentWindow, "InventoryEquipmentWindow.svg"

const SLOT_TYPE = preload('../constants.gd').SLOT_TYPE

signal item_set(item, from, to)
signal item_removed(item, from, to)
signal item_drag_started(item)
signal item_drag_forbidden(item)

var textureRect: TextureRect

export(Texture) var texture setget set_texture, get_texture

func set_texture(new_texture: Texture) -> void:
	if textureRect:
		textureRect.texture = new_texture

func get_texture() -> Texture:
	if textureRect:
		return textureRect.texture
	return null

export(int) var slot_edge_size:int = 32 setget set_slot_edge_size, get_slot_edge_size

func set_slot_edge_size(size: int) -> void:
	if not is_inside_tree(): yield(self, "ready")
	slot_edge_size = size
	rect_min_size = Vector2(size, size)
	for slot in get_children():
		slot.edge_size = size

func get_slot_edge_size() -> int:
	return slot_edge_size
	
func _init_node_tree():
	textureRect = _set_container(self, TextureRect.new())
	textureRect.stretch_mode = TextureRect.STRETCH_KEEP_CENTERED
	textureRect.size_flags_horizontal = SIZE_EXPAND_FILL
	textureRect.size_flags_vertical = SIZE_EXPAND_FILL
	
	var rows = _set_container(self, VBoxContainer.new(), 10)
	
	for type_name in SLOT_TYPE:
		var type: int = SLOT_TYPE[type_name]
		if type == SLOT_TYPE.ANY:
			continue
		var slot = InventoryItemSlot.new()
		slot.item_owner = self
		slot.name = 'slot_'+type_name
		slot.edge_size = slot_edge_size
		slot.slot_type = type
		assert(slot.connect("item_set", self, "_on_item_set") == OK)
		assert(slot.connect("item_removed", self, "_on_item_removed") == OK)
		assert(slot.connect("item_drag_started", self, "_on_item_drag_started") == OK)
		assert(slot.connect("item_drag_forbidden", self, "_on_item_drag_forbidden") == OK)
		var hbox = HBoxContainer.new()
		var label = Label.new()
		label.text = type_name
		hbox.add_child(slot)
		hbox.add_child(label)
		rows.add_child(hbox)
	
func _on_item_removed(item: InventoryItem):
	emit_signal("item_removed", item)

func _on_item_set(item: InventoryItem):
	emit_signal("item_set", item)

func _on_item_drag_started(item: InventoryItem):
	emit_signal("item_drag_started", item)

func _on_item_drag_forbidden(item: InventoryItem):
	emit_signal("item_drag_forbidden", item)

func _init():
	_init_node_tree()
