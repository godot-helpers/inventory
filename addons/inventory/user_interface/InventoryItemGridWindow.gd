tool
extends InventoryWindowDialog
class_name InventoryItemGridWindow, "InventoryItemGridWindow.svg"

################################################################################
# 
#          ITEM GRID
# A popup window with a resizeable grid. Handles adding and removing items
# 
################################################################################

################################################################################
# 
# SIGNALS
# 

signal item_set(item)
signal item_removed(item)
signal item_drag_started(item)
signal item_drag_forbidden(item)

################################################################################
# 
# SLOTS HANDLING
# 

# This adds an item if it cans. Returns `false` if it fails
func add_item(item: InventoryItem) -> bool:
	return gridContainer.add_item(item)

# relays the item removed signal from one of the slots
func _on_item_removed(item: InventoryItem):
	# warning-ignore:return_value_discarded
	emit_signal("item_removed", item)

# relays the item added signal from one of the slots
func _on_item_set(item: InventoryItem):
	# warning-ignore:return_value_discarded
	emit_signal("item_set", item)

func _on_item_drag_started(item: InventoryItem):
	emit_signal("item_drag_started", item)

func _on_item_drag_forbidden(item: InventoryItem):
	emit_signal("item_drag_forbidden", item)

################################################################################
# 
# PROXY VARS
# 

export(Array, Resource) var items = [] setget set_items_list, get_items_list

# setter for the items list
func set_items_list(list: Array) -> void:
	gridContainer.items = list

# getter for the items list
func get_items_list() -> Array:
	return gridContainer.items


export(int,1,50) var columns = 8 setget set_columns, get_columns

func set_columns(new_columns: int) -> void:
	gridContainer.columns = new_columns

func get_columns() -> int:
	return gridContainer.columns

export(int, 8, 100, 8) var countenance = 48 setget set_countenance, get_countenance

func set_countenance(new_countenance:int) -> void:
	gridContainer.countenance = new_countenance

func get_countenance() -> int:
	return gridContainer.countenance

export(bool) var allow_drag setget set_allow_drag, get_allow_drag

func set_allow_drag(value: bool) -> void:
	gridContainer.allow_drag = value
func get_allow_drag() -> bool:
	return gridContainer.allow_drag

export(int) var slot_edge_size:int setget set_slot_edge_size, get_slot_edge_size

func set_slot_edge_size(size: int) -> void:
	gridContainer.slot_edge_size = size

func get_slot_edge_size() -> int:
	return gridContainer.slot_edge_size

func consume_first_item_of_type(type: String) -> bool:
	return gridContainer.consume_first_item_of_type(type)

func consume_recipe(recipe) -> bool:
	return gridContainer.consume_recipe(recipe)

################################################################################
# 
# NODE TREE
# 

var gridContainer: InventoryItemGrid = InventoryItemGrid.new()

func _init_node_tree() -> void:

	var marginContainer = _set_container(self, MarginContainer.new(), 10)
	var scrollContainer = _set_container(marginContainer, ScrollContainer.new())
	
	gridContainer	= _set_container(scrollContainer, gridContainer)
	gridContainer.connect("item_set", self, "_on_item_set")
	gridContainer.connect("item_removed", self, "_on_item_removed")
	gridContainer.connect("item_drag_started", self, "_on_item_drag_started")
	gridContainer.connect("item_drag_forbidden", self, "_on_item_drag_forbidden")

################################################################################
# 
# BOOTSTRAPPING
# 

func _init():
	_init_node_tree()