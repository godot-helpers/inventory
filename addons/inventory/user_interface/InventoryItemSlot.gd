tool
extends Panel
class_name InventoryItemSlot, "InventoryItemSlot.svg"

################################################################################
# 
#          ITEM SLOT
# A simple Control that handles accepting an Item, or removing an item
# 
################################################################################

################################################################################
# 
# SIGNALS
#

signal item_set(item)
signal item_removed(item)
signal item_drag_started(item)
signal item_drag_forbidden(item)

################################################################################
# 
# META INFORMATION
#

var index:int = 0
var has_item := false setget, _get_has_item
var is_free := true setget, _get_is_free

func _get_has_item() -> bool:
	return item != null
	
func _get_is_free() -> bool:
	return item == null

################################################################################
# 
# ITEM ADDING AND REMOVING
#

var item_owner: Control #: InventoryWindowDialog <-- cyclic check

# Handles setting an item. The `item_owner` property MUST BE SET on the slot
# before adding an item
func set_item(_item: InventoryItem) -> void:
	#assert(item_owner != null, "Slot %s has no Item Owner. `item_owner` must be set before calling `set_item`"%[self])
	if not _item or _item == item: return
	if not is_inside_tree(): yield(self, "ready")
	var _is_free = _get_is_free()
	assert(_is_free, "slot isn't free: %s"%[item])
	if _is_free:
		item = _item
		var texture = item.texture 
		textureContainer.texture = texture
		if not Engine.is_editor_hint():
			item.emit_signal("moved")
			emit_signal("item_set", item)
			item.connect("moved", self, "_on_item_moved",[self])

func get_item():
	return item

# Checks that the slot can accept the item
func is_valid_item(_item: InventoryItem) -> bool:
	return \
		_get_is_free() and \
		('is_item' in _item or item is InventoryItem) and \
		slot_types_match(_item)

# Checks that an item's slot type matches with slot's slot type
func slot_types_match(_item: InventoryItem) -> bool:
	return slot_type == SLOT_TYPE.ANY || _item.slot_type == slot_type

# Removes the item from the slot
func remove_item() -> InventoryItem:
	if item != null:
		var _item = item;
		if not Engine.is_editor_hint():
			_item.disconnect("moved", self, "_on_item_moved")
		item = null
		textureContainer.texture = null
		if not Engine.is_editor_hint():
			#yield(get_tree(),"idle_frame")
			emit_signal("item_removed", _item)
		return _item
	return null

# Called when the item is moved in or out
func _on_item_moved(new_location):
	remove_item()

################################################################################
# 
# DRAG AND DROP HANDLING
# 

# Returns drag data when the user starts dragging. If there's an item, and 
# `allow_drag` is true, then the item is returned. Otherwise, the drag is either
# set as "forbidden" (if there _is_ an item, but `allow_drag` is false), or 
# is just cancelled
func get_drag_data(_position: Vector2):
	if item:
		if allow_drag == true :
			emit_signal("item_drag_started", item)
			return item
		else:
			emit_signal("item_drag_forbidden", item)
			# TODO: return something sensible here
			return 'forbidden'
	return null

# Tests the dropped data and checks if the drop data is allowed
func can_drop_data(_position: Vector2, data):
	return data != null and is_valid_item(data)

# After passing the `can_drop_data` check, the item is set
func drop_data(_position: Vector2, data: InventoryItem):
	# TODO: extract this as a signal and handle this in user code, in order to 
	# allow combinations
	set_item(data)

################################################################################
# 
# SETTING UP NODE TREE AND PROXIES
#

var container:MarginContainer = MarginContainer.new()
var textureContainer: TextureRect = TextureRect.new()

func _init_node_tree():
	.add_child(container)
	container.size_flags_horizontal = SIZE_EXPAND_FILL
	container.size_flags_vertical = SIZE_EXPAND_FILL
	container.mouse_filter = MOUSE_FILTER_IGNORE
	textureContainer.size_flags_horizontal = SIZE_EXPAND_FILL
	textureContainer.size_flags_vertical = SIZE_EXPAND_FILL
	textureContainer.expand = true
	textureContainer.stretch_mode = TextureRect.STRETCH_KEEP_ASPECT_CENTERED
	textureContainer.mouse_filter = MOUSE_FILTER_IGNORE
	container.add_child(textureContainer)
	_draw_node_tree()

func _draw_node_tree():
	container.rect_size = rect_size
	container.rect_min_size = rect_size

func add_child(node: Node, legible_unique_name:=false) -> void:
	container.add_child(node, legible_unique_name)

# Overriding remove_child
func remove_child(node):
	container.remove_child(node)

var texture setget set_texture, get_texture

func set_texture(tex: Texture) -> void:
	if not is_inside_tree(): yield(self, "ready")
	textureContainer.texture = tex

func get_texture() -> Texture:
	return textureContainer.texture if textureContainer else null
	var a = TextureRect.STRETCH_KEEP

func set_stretch_mode(stretch: int) -> void:
	if not is_inside_tree(): yield(self, "ready")
	textureContainer.stretch_mode = stretch

func get_stretch_mode() -> int:
	return textureContainer.stretch_mode if textureContainer else STRETCH_MODE.STRETCH_KEEP

func set_edge_size(new_size: float) -> void:
	var v = Vector2(new_size, new_size)
	rect_size = v
	rect_min_size = v
	
func get_edge_size() -> float:
	return rect_size.x

################################################################################
# 
# BOOTSTRAPPING
#
func _init():
	_init_node_tree()

func _draw():
	_draw_node_tree()

################################################################################
# 
# CONSTANTS FOR TYPING
#

const SLOT_TYPE = preload('../constants.gd').SLOT_TYPE
const STRETCH_MODE = preload('../constants.gd').STRETCH_MODE
# DEL: onready var Inventory = preload('../utils.gd').get_inventory_singleton(self)

################################################################################
# 
# EXPORTS
#

export(Resource) var item setget set_item, get_item

# Sets the Slot's slot type. This type has to either be `any`, or match
# the item's slot type
export(SLOT_TYPE) var slot_type = SLOT_TYPE.ANY

# If this is set to `false`, the user won't be able to drag an item out
export(bool) var allow_drag = true

export(STRETCH_MODE) var stretch_mode setget set_stretch_mode, get_stretch_mode

export(int) var edge_size = 64 setget set_edge_size, get_edge_size
