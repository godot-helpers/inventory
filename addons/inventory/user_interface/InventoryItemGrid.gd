tool
extends GridContainer
class_name InventoryItemGrid, "InventoryItemGrid.svg"

################################################################################
# 
#          ITEM GRID
# A popup window with a resizeable grid. Handles adding and removing items
# 
################################################################################

################################################################################
# 
# SIGNALS
# 

signal item_set(item)
signal item_removed(item)
signal item_drag_started(item)
signal item_drag_forbidden(item)

################################################################################
# 
# DRAGGING AND DROPPING
# 

export(bool) var allow_drag = true setget set_allow_drag, get_allow_drag

func set_allow_drag(value: bool) -> void:
	if not is_inside_tree(): yield(self, "ready")
	allow_drag = value
	for slot in get_children():
		slot.allow_drag = value

func get_allow_drag() -> bool:
	return allow_drag

export(int) var slot_edge_size:int = 64 setget set_slot_edge_size, get_slot_edge_size

func set_slot_edge_size(size: int) -> void:
	if not is_inside_tree(): yield(self, "ready")
	slot_edge_size = size
	rect_min_size = Vector2(size, size)
	for slot in get_children():
		slot.edge_size = size
	shrink_size()

func get_slot_edge_size() -> int:
	return slot_edge_size

################################################################################
# 
# SLOTS HANDLING
# 
#
# This is a list of items that must be set before the game runs
# this not to be used during runtime. The items get duplicated every time
# the array is mutated
#

export(Array, Resource) var items = [] setget set_items_list, get_items_list

func is_item(item) -> bool:
	return 'is_item' in item

# setter for the items list
func set_items_list(list: Array) -> void:
	items = []
	for item in list:
		if item == null:
			items.push_back(item)
		elif is_item(item):
			items.push_back(item.duplicate())
	if Engine.is_editor_hint() or not _item_list_was_set_once:
		set_items_from_items_array(list, true)

# getter for the items list
func get_items_list() -> Array:
	return items

var _item_list_was_set_once = false

# Takes a list of items resources and sets them in the grid. The optional 
# `expand` parameter will automatically grow the table to accomodate items
# slots will be added in order to form uniform rows, so they'll depend on
# columns. Set columns first if the default (8) isn't your preference
func set_items_from_items_array(list: Array, expand:=false) -> void:
	_item_list_was_set_once = true
	var children = get_children()
	var children_count = get_child_count()
	var count = min(list.size(), children_count)
	var diff = children_count - list.size()
	if diff < 0 and expand:
		var additional = int(ceil(columns/diff)) * -1 * columns
		set_countenance(get_countenance() + additional)
	for i in range(count):
		var slot: InventoryItemSlot = children[i]
		var item = list[i]
		if slot == null:
			return
		slot.remove_item()
		if item != null:
			var _is_item = is_item(item)
			assert(_is_item, "item '%s' isn't an Item Resource"%[item])
			if _is_item:
				slot.set_item(item.duplicate())
	if diff > 0:
		for i in range(diff):
			var index = i + list.size()
			var slot: InventoryItemSlot = children[index]
			slot.remove_item()

# This adds an item if it cans. Returns `false` if it fails
func add_item(item: InventoryItem) -> bool:
	var next_slot:InventoryItemSlot = _find_next_slot()
	if next_slot:
		next_slot.set_item(item)
		return true
	return false

# Find the next free slot, if there is any
func _find_next_slot(slot:InventoryItemSlot = null) -> InventoryItemSlot:
	if slot and not slot.has_item:
		return slot
	for next_slot in get_children():
		if not next_slot.has_item:
			return next_slot
	return null

# relays the item removed signal from one of the slots
func _on_item_removed(item: InventoryItem, slot: InventoryItemSlot):
	# warning-ignore:return_value_discarded
	if _is_ready:
		items[slot.index] = null
	emit_signal("item_removed", item)

# relays the item added signal from one of the slots
func _on_item_set(item: InventoryItem, slot: InventoryItemSlot):
	# warning-ignore:return_value_discarded
	if _is_ready:
		items[slot.index] = item
	emit_signal("item_set", item)

func _on_item_drag_started(item: InventoryItem):
	emit_signal("item_drag_started", item)

func _on_item_drag_forbidden(item: InventoryItem):
	emit_signal("item_drag_forbidden", item)

################################################################################
# 
# EDITOR STUFF
# 

# Determines how many slots exist in the grid
# by default, steps by multiples of 8, because the grid has 8 columns.
# If the amount of columns in the grid changes, change it here too.
export(int, 8, 100, 1) var countenance = 48 setget set_countenance, get_countenance

# this tracks if the items that people may have been set as children of the
# window have been looped through and put in the grid.
# It starts as false as to not run before _ready() is called (since this
# is a `tool` script).
# _ready() set is to true, then, after the items have been ordered, it's set to
# false again 
var _editor_items_not_placed_yet = false

# Sets the max amount of items a grid can hold
# This shouldn't be used at runtime (the window doesn't handle dynamic
# resizes)
# Once the number is set, it syncs the amount of slots (adds or removes slots)
# then, if not running as a `tool`, places the children items of the window in 
# the grid
func set_countenance(new_countenance:int) -> void:
	countenance = new_countenance
	if not is_inside_tree(): yield(self, 'ready')
	sync_slots()

func get_countenance() -> int:
	return countenance

func set_columns(columns: int) -> void:
	.set_columns(columns)
	if not is_inside_tree(): yield(self, "ready")
	yield(get_tree(), "idle_frame")
	shrink_size()

# This is used both on _ready(), and by the editor, to change the amount of 
# slots dynamically, depending on the max amount of allowed objects
func sync_slots() -> void:
	var children_count = get_child_count()
	var diff: int = children_count - get_countenance()
	if diff == 0:
		return
	if diff < 0:
		for i in range(diff * -1):
			var slot: InventoryItemSlot = InventoryItemSlot.new()
			#slot.item_owner = self
			slot.allow_drag = allow_drag
			slot.edge_size = slot_edge_size
			slot.index = i
			assert(slot.connect("item_set", self, "_on_item_set", [slot]) == OK)
			assert(slot.connect("item_removed", self, "_on_item_removed", [slot]) == OK)
			assert(slot.connect("item_drag_started", self, "_on_item_drag_started") == OK)
			assert(slot.connect("item_drag_forbidden", self, "_on_item_drag_forbidden") == OK)
			add_child(slot, true)
	elif diff > 0:
		var children = get_children()
		for i in range(diff):
			var child_index = children_count - i - 1
			var child = children[child_index]
			child.queue_free()
	items.resize(get_countenance())
	shrink_size()

# sets the size to shrink to match children
func shrink_size():
	if not is_inside_tree(): yield(self, "ready")
	yield(get_tree(),"idle_frame")
	rect_size = Vector2.ZERO

func consume_first_item_of_type(item_type: String) -> bool:
	for slot in get_children():
		if slot.has_item and slot.item.type == item_type:
			slot.remove_item()
			return true
	return false 

func consume_recipe(recipe) -> bool:
	for ingredient in recipe.ingredients:
		for n in range(ingredient.amount):
			var type = ingredient.item.type
			if not consume_first_item_of_type(type):
				return false
	var item = recipe.result.duplicate()
	add_item(item)
	return true

################################################################################
# 
# BOOTSTRAPPING
# 

var _is_ready = false

func _ready():
	_on_ready_verify_children()
	_is_ready = true


func _init():
	if columns <= 1:
		columns = 8
	sync_slots()

func _on_ready_verify_children():
	if OS.is_debug_build():
		for child in get_children():
			assert(child is InventoryItemSlot)
