enum SLOT_TYPE {
	ANY,
	HEAD,
	BODY,
	FEET,
	LEGS,
	NECK,
	TRINKET_1,
	TRINKET_2,
	HAND_LEFT,
	HAND_RIGHT
}

enum STRETCH_MODE{
	SCALE_ON_EXPAND, # — Scale to fit the node’s bounding rectangle, only if expand is true. Default stretch_mode, for backwards compatibility. Until you set expand to true, the texture will behave like STRETCH_KEEP.
	SCALE, #Scale to fit the node’s bounding rectangle.
	TILE, #Tile inside the node’s bounding rectangle.
	KEEP, #The texture keeps its original size and stays in the bounding rectangle’s top-left corner.
	KEEP_CENTERED, #The texture keeps its original size and stays centered in the node’s bounding rectangle.
	KEEP_ASPECT, #Scale the texture to fit the node’s bounding rectangle, but maintain the texture’s aspect ratio.
	KEEP_ASPECT_CENTERED, #Scale the texture to fit the node’s bounding rectangle, center it and maintain its aspect ratio.
	KEEP_ASPECT_COVERED #Scale the texture so that the shorter side fits the bounding rectangle. The other side clips to the node’s limits.
}
