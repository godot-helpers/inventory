extends Resource
class_name InventoryRecipe, "InventoryRecipe.svg"

const is_recipe = true

export(Resource) var result setget set_result
export(Array, Resource) var ingredients setget set_ingredients_list, get_ingredients_list

func set_result(_item: Resource):
	assert(_item is InventoryItem)
	result = _item

func get_result() -> Resource:
	return result

# setter for the ingredients list
func set_ingredients_list(list: Array) -> void:
	ingredients = []
	for item in list:
		assert(item is InventoryIngredient or item == null, "item %s is not a valid ingredient"%[item])
		ingredients.push_back(item.duplicate())

# getter for the ingredients list
func get_ingredients_list() -> Array:
	return ingredients

func _to_string():
	return "(recipe for %s %s)"%[result.type, ingredients]