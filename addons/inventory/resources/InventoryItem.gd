tool
extends Resource
class_name InventoryItem, "InventoryItem.svg"

const SLOT_TYPE = preload('../constants.gd').SLOT_TYPE
const is_item = true


signal moved

export(String) var name: String = "item"
export(String) var type: String = "item"
export(Texture) var texture: Texture
export(SLOT_TYPE) var slot_type = SLOT_TYPE.ANY

func _to_string():
	return "(item %s)"%[name]
