extends Resource
class_name InventoryIngredient, "InventoryIngredient.svg"

export(Resource) var item setget set_item, get_item
export(int) var amount = 1
var type:String setget, get_type

func set_item(_item: Resource):
	assert(_item is InventoryItem)
	item = _item.duplicate()

func get_item() -> Resource:
	return item

func _to_string():
	return "(Ingredient %s %s)"%[item.name, amount]

func get_type() -> String:
	return item.type