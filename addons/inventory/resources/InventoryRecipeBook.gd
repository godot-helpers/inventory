extends Node
class_name InventoryRecipeBook, "InventoryRecipeBook.gd"

export(Array, Resource) var recipes

func get_recipes_available(items: Array):
	var ingredients_dict = {}
	for i in range(items.size()):
		var item: InventoryItem = items[i]
		if item == null: 
			continue;
		var type = item.type
		if not type in ingredients_dict:
			var ingredient = InventoryIngredient.new()
			ingredient.item = item
			ingredient.amount = 1
			ingredients_dict[type] = ingredient
		else:
			ingredients_dict[type].amount += 1
	
	var recipes_available = []
	for i in range(recipes.size()):
		var recipe: InventoryRecipe = recipes[i]
		var valid = true
		var j = 0
		while j < recipe.ingredients.size() and valid:
			var ingredient: InventoryIngredient = recipe.ingredients[j]
			j+=1
			var type = ingredient.type
			if not type in ingredients_dict or ingredients_dict[type].amount < ingredient.amount:
				valid = false
		if valid == true:
			recipes_available.push_back(recipe)
	return recipes_available
