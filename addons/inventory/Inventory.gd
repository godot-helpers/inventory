extends CanvasLayer

################################################################################
# 
#           INVENTORY SYSTEM
# Handles everything related to the inventory. Meant to be used as a singleton
# 
################################################################################

################################################################################
# 
# GENERAL WINDOW HANDLING
#

# Creates a window programatically and returns it. The last parameter allows
# to pass any property that a window accepts 
func create_window(title:String, contents: Array, props: Object = null) -> InventoryItemGridWindow:
	var window: InventoryItemGridWindow = InventoryItemGridWindow.new()
	# warning-ignore:return_value_discarded
	var countenance = props.countenance if props and 'countenance' in props else contents.size()
	window.countenance = countenance
	window.window_title = title
	add_window(window)
	if props:
		for property in props:
			if \
			property !='window_title' and \
			property != 'countenance' and \
			property in window:
				window[property] = props[property]
	for item in contents:
		assert(item is InventoryItem, "item %s isn't an instance of InventoryItem"%[item])
		# warning-ignore:return_value_discarded
		window.add_item(item)
	return window

# Appends a window to the window list, and opens it
func add_window(window: InventoryWindowDialog, show: bool = false) -> InventoryWindowDialog:
	var parent = window.get_parent()
	if parent:
		parent.remove_child(window)
	windows_layer.add_child(window)
	window.visible = show
	# warning-ignore:return_value_discarded
	window.connect("gui_input", self, "_on_gui_input", [window])
	return window

# Makes the window visible and on top
func show_window(window: InventoryWindowDialog, center: bool = true) -> InventoryWindowDialog:
	window.visible = true
	_set_current_window(window)
	if center:
		find_position(window)
	return window

# Makes the current window visible and on top if it is hidden
func toggle_window(window: InventoryWindowDialog, center: bool = false) -> InventoryWindowDialog:
	if window.visible:
		window.visible = false
	else:
		# warning-ignore:return_value_discarded
		show_window(window,center)
	return window

# Moves the window on top
func _set_current_window(window: InventoryWindowDialog):
	windows_layer.move_child(window, windows_layer.get_child_count())

# Called when the mouse or finger is over a window. Brings it to top by default
func _on_gui_input(event: InputEvent, window: InventoryWindowDialog) -> void:
	if event is InputEventMouseButton or event is InputEventScreenTouch:
		_set_current_window(window)
	if event is InputEventMouseMotion or event is InputEventScreenDrag:
		_set_current_window(window)

func find_position(window: InventoryWindowDialog) -> void:
	window.center()
	var windows = windows_layer.get_children()
	for next_window in windows:
		var pos = window.rect_position.floor()
		if next_window == window or next_window.visible == false: 
			continue
		var diff = (next_window.rect_position.floor() - pos).abs()
		if diff < Vector2(10,10):
			window.rect_position += Vector2(10,10)

################################################################################
# 
# DRAG N DROP HANDLING
#


# Adds an Item to the top layer
# the item will be copied
# the item will be set to transparent 
func set_drag_preview(control: Control) -> void:
	var preview = control.duplicate()
	preview.modulate.a = 0.5
	overlay_layer.set_drag_preview(preview)

func set_drag_preview_item(item: InventoryItem):
	set_drag_preview_texture(item.texture)

func set_drag_preview_texture(texture: Texture):
	var tex = TextureRect.new()
	tex.texture = texture
	tex.modulate.a = 0.5
	overlay_layer.set_drag_preview(tex)


################################################################################
# 
# BOOTSTRAPPING
#

# This layer will be over the whole game, and show the windows
onready var windows_layer: Control = Control.new()

# This layer will be over the windows layer, it shows the drag overlay icon
onready var overlay_layer: Control = Control.new()

func _ready():
	windows_layer.name = "windows_layer"
	overlay_layer.name = "overlay_layer"
	add_child(windows_layer)
	add_child(overlay_layer)
	overlay_layer.size_flags_horizontal = Control.SIZE_EXPAND_FILL
	overlay_layer.size_flags_vertical = Control.SIZE_EXPAND_FILL

################################################################################
# 
# UTILITIES
#

static func get_dir_contents(path: String, fn: FuncRef, recursion = 0) -> void:
	path = path.rstrip("/") + "/"
	var dir = Directory.new()
	if dir.open(path) == OK:
		dir.list_dir_begin(true, true)
		var file_name = dir.get_next()
		while file_name != "":
			var full_path = path + file_name
			if dir.current_is_dir():
				if recursion > 0:
					get_dir_contents(full_path, fn, recursion - 1)
			else:
				fn.call_func(full_path)
			file_name = dir.get_next()
	else:
		print("An error occurred when trying to access the path.")

class FileArray:
	var extensionFilter: RegEx = RegEx.new()
	var files = PoolStringArray()
	var expression: Expression = Expression.new()
	var _do_load: bool = false
	var _has_condition: bool = false
	func _init(ext: String, condition: String = "", do_load:bool = false):
		assert(extensionFilter.compile("\\."+ext+"$") == OK, "could not compile regex")
		_do_load = do_load
		if condition != "":
			_has_condition = true
			var args = ["path","scene"] if _do_load else ["path"]
			assert(expression.parse(condition,args) == OK, "could not compile expression")
	func add_item(file_path:String):
		if extensionFilter.search(file_path):
			if not _has_condition:
				files.append(file_path)
			else:
				var args = [file_path, load(file_path)] if _do_load else [file_path]
				var result = expression.execute(args, self, true)
				if not expression.has_execute_failed() and result:
					files.append(file_path)

static func get_all_recipes_in_directory(path:String, recursion := 0) -> InventoryRecipeBook:
	var arr = FileArray.new("tres", "'is_recipe' in scene", true)
	var fn = funcref(arr, "add_item")
	get_dir_contents(path, fn, recursion)
	var book: InventoryRecipeBook = InventoryRecipeBook.new()
	for file in arr.files:
		var recipe: InventoryRecipe = load(file)
		if recipe.result == null:
			continue
		var ingredients = []
		for ingredient in recipe.ingredients:
			if ingredient == null or ingredient.item == null or ingredient.amount == null:
				continue
			ingredients.push_back(ingredient)
		if ingredients.size() > 0:
			recipe.ingredients = ingredients
			book.recipes.push_back(recipe)
	return book
