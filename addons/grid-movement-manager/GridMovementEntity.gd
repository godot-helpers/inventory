extends Reference
class_name GridMovementEntity

var target_grid_position := Vector2(0,0)
var speed_modifier := 0.0
var direction := Vector2()

export var grid_position := Vector2(0,0)

# The time needed to do one step, in seconds
export var step_time:= .3
export var base_speed := 1
export var is_solid := true

export(int, -1, 1) var direction_x setget set_direction_x, get_direction_x

func set_direction_x(val:int) -> void:
	direction.x = val

func get_direction_x() -> int:
	return int(direction.x)

export(int, -1, 1) var direction_y setget set_direction_y, get_direction_y

func set_direction_y(val:int) -> void:
	direction.y = val

func get_direction_y() -> int:
	return int(direction.y)

var is_moving: bool setget , get_is_moving

func get_is_moving() -> bool:
	return target_grid_position != grid_position

var node: Node2D

func _init(parent: Node2D):
  node = parent