###############################################################################
#
# SCREEN JOYSTICK
#
# Uses the screen to determine movement. Maps cursor position to 8 cardinal
# directions, and to 3 tiers of distance.
# Dispatches a signal every time distance of angle change.
#
# This resource does _not_ tap into the input handling on its own, it just
# provides convenience methods; it's up to you to wire it into the input with
# something like:
# ```
# func _input(event):
#   var has_changed = thisRef.process_cursor(event.position, get_viewport_rect().size)
#   if has_changed:
#     print(thisResouce.distance, ", ", thisRef.direction)
# ```
#
# Optionally, give it an `offset` to align the movement better
# with `thisReg.offset = Vector2(0,32)`.
# 
# This class also dispatches a `cursor_changed` signal when the distance or the
# angle change
extends Reference

signal cursor_changed(distance, direction)

const DISTANCES = {
	"FAR": "far",
	"CLOSE": "close",
	"MEDIUM": "med"
}
const DIRECTIONS = {
	"NORTH":"n",
	"SOUTH":"s",
	"EAST":"e",
	"WEST":"w",
	"NORTHEAST":"ne",
	"NORTHWEST":"nw",
	"SOUTHEAST":"se",
	"SOUTHWEST":"sw",
}

var offset: Vector2 = Vector2(0,32)
var distance:String = DISTANCES.STOPPED
var direction:String = DIRECTIONS.NORTH

# Processes the cursor's distance and angle from the center of the screen
# Return `true` if the angle and/or the distance have changed since the
# last call
# @param cursor_position the position of the event, presumably gotten through
#        event.position
# @param area_size the screen size, presumably gotten through 
#        get_viewport_rect().size
func process_cursor(cursor_position:Vector2, area_size: Vector2) -> bool:
	var character_pos = cursor_position + offset
	var half_size = area_size / 2
	var angle_has_changed = _process_cursor_angle(half_size, character_pos)
	var distance_has_changed = _process_distance(half_size, character_pos)
	if angle_has_changed or distance_has_changed:
		emit_signal("cursor_changed", distance, direction)
		return true
	return false

# Calculates the angle of the cursor, relative to a point in the middle of the 
# screen. Discretizes the angle to 8 directions.
# Returns true if the angle has changed
func _process_cursor_angle(area_size: Vector2, character_pos: Vector2) -> bool:
	var exact_angle = rad2deg((area_size).angle_to_point(character_pos))
	var rounded_angle = int(round(exact_angle/45)*45)
	var previous_direction = direction

	#var cursor_direction = Vector2() 
	# This is actually not needed anymore, but it took some time to
	# map cardinals with vectors, so I'm keeping it a bit longer

	match rounded_angle:
		0:
			#cursor_direction = Vector2(-1,1)
			direction = DIRECTIONS.WEST
		-45:
			#cursor_direction = Vector2(0,1)
			direction = DIRECTIONS.SOUTHWEST
		-90:
			#cursor_direction = Vector2(1,1)
			direction = DIRECTIONS.SOUTH
		-135:
			#cursor_direction = Vector2(1,0)
			direction = DIRECTIONS.SOUTHEAST
		180, -180:
			#cursor_direction = Vector2(1,-1)
			direction = DIRECTIONS.EAST
		135:
			#cursor_direction = Vector2(0,-1)
			direction = DIRECTIONS.NORTHEAST
		90:
			#cursor_direction = Vector2(-1,-1)
			direction = DIRECTIONS.NORTH
		45:
			#cursor_direction = Vector2(-1,0)
			direction = DIRECTIONS.NORTHWEST
	return direction != previous_direction

# Calculates how far the cursor is from the character, and maps the distance
# to three tiers
# Returns `true` if the cursor distance has changed
func _process_distance(area_size:Vector2, character_pos: Vector2) -> bool:
	var curs = ((( character_pos.abs() - area_size ) / area_size * -1) * 10).floor().abs()
	var x = curs.x
	var y = curs.y
	var previous_distance = distance
	if x >= 8 or y >= 8:
		distance = DISTANCES.FAR
	elif x <= 1 and y <= 1:
		distance = DISTANCES.CLOSE
	else:
		distance = DISTANCES.MEDIUM
	return previous_distance != distance