extends Node

const GridMovementEntity = preload('./GridMovementEntity.gd')

var tileMap: TileMap
var _nodes := {}
var entities := {}
var _tile_offset = null

func register(node: Node2D, use_original_position:= true):
	var entity = GridMovementEntity.new(node)
	_nodes[node] = entity
	if use_original_position:
		snap_entity_to_grid(node)
	set_entity_position(entity)
	return entity

func unregister(node: Node2D):
	var entity: GridMovementEntity = get_entity(node)
	erase_entity_position(entity)
	return _nodes.erase(node)

func get_entity(node: Node2D) -> GridMovementEntity:
	assert(_nodes.has(node))
	return _nodes[node]

func snap_entity_to_grid(node: Node2D):
	var target_grid_position = world_to_map(node.position)
	var entity: GridMovementEntity = get_entity(node)
	entity.grid_position = target_grid_position
	entity.target_grid_position = target_grid_position
	node.position = map_to_world(target_grid_position)

func set_entity_position(entity: GridMovementEntity):
	entities[entity.grid_position] = entity
	entities[entity.target_grid_position] = entity
	
func erase_entity_position(entity: GridMovementEntity):
	var erased_pos = false
	var erased_target_pos = false
	if entities.has(entity.grid_position):
		erased_pos = entities.erase(entity.grid_position)
	if entities.has(entity.target_grid_position):
		erased_target_pos = entities.erase(entity.target_grid_position)
	return erased_pos and erased_target_pos
	
func get_tileMap_offset() -> Vector2:
	if _tile_offset == null:
		assert(tileMap != null, "a tilemap should be set")
		if tileMap.mode == TileMap.MODE_SQUARE:
			_tile_offset = Vector2.ZERO
		else:
			_tile_offset = Vector2(0, tileMap.cell_size.y / 2)
	return _tile_offset

static func cartesian_to_isometric(vector: Vector2) -> Vector2:
	return Vector2(vector.x - vector.y, (vector.x + vector.y) / 2)

func is_cell_vacant(pos=Vector2()) -> bool:
	var entity: GridMovementEntity = get_entity_at_cell(pos)
	if entity != null:
		if entity.is_solid:
			return false
	return tileMap.get_cellv(pos) == -1

func get_entity_at_cell(pos: Vector2) -> GridMovementEntity:
	if entities.has(pos):
		return entities[pos]
	return null

func map_to_world(map_position: Vector2, ignore_half_ofs: bool = false) -> Vector2:
	return tileMap.map_to_world(map_position, ignore_half_ofs) + _tile_offset

func world_to_map(pos: Vector2) -> Vector2:
	return tileMap.world_to_map(pos + get_tileMap_offset())

func snap_to_grid(pos: Vector2) -> Vector2:
	return tileMap.map_to_world(tileMap.world_to_map(pos))

func _process(_delta):
	for node in _nodes:
		process_managed_node(node)

func process_managed_node(node: Node2D) -> void:
	var entity: GridMovementEntity = get_entity(node)
	var direction = entity.direction
	var speed_factor = entity.speed_modifier
	if speed_factor == 0 or entity.is_moving or direction == Vector2():
		return
	var target_direction = direction.normalized().round()
	var target_grid_position = entity.grid_position + direction
	if is_cell_vacant(target_grid_position):
		entity.direction = target_direction
		entity.target_grid_position = target_grid_position
		move_entity(node, entity)

func move_entity(node: Node2D, entity: GridMovementEntity):
	erase_entity_position(entity)
	var transition = Tween.TRANS_QUAD
	var easing = Tween.EASE_IN_OUT
	var target = map_to_world(entity.target_grid_position)
	var speed = entity.step_time * entity.speed_modifier
	var start = node.position
	var tween = get_move_tween_for_node(node, entity)
	tween.stop_all()
	tween.interpolate_property(node, "position", start, target, speed, transition, easing)
	set_entity_position(entity)
	tween.start()

func get_move_tween_for_node(node: Node2D, entity: GridMovementEntity) -> Tween:
	if not node.has_node("grid_manager_tween"):
		var tween = Tween.new()
		tween.name = "grid_manager_tween"
		tween.connect("tween_completed", self, "_on_tween_completed", [entity, tween])
		node.add_child(tween, true)
	return node.get_node("grid_manager_tween") as Tween

func _on_tween_completed(node: Node2D, _key, entity: GridMovementEntity, tween: Tween):
	erase_entity_position(entity)
	entity.grid_position = entity.target_grid_position
	entity.speed_modifier = 0
	var _dismiss = tween.stop_all()
	set_entity_position(entity)
