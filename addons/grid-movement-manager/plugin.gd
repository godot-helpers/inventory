tool
extends EditorPlugin

const AUTOLOAD_NAME = "GridMovementManager"
const base_path = 'GridMovementManager.gd'
var path = ""

func _enter_tree():
	var packedScene = preload(base_path)
	var scene = packedScene.new()
	path = scene.get_script().get_path()
	add_autoload_singleton(AUTOLOAD_NAME, path)

func _exit_tree():
	remove_autoload_singleton(path)
