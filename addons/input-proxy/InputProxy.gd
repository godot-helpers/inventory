class_name InputProxy

# Returns true if the event is a mouse click or a tap
static func is_tap_event(event: InputEvent) -> bool:
	return event is InputEventMouseButton or event is InputEventScreenTouch

# Returns true if the event is a mouse move or a touch drag
static func is_move_event(event: InputEvent) -> bool:
	return event is InputEventMouseMotion or event is InputEventScreenDrag

# Helper method that maps a vector between 0 and 1 to a location relative
# to the given sprite
static func unit_vector_to_location(vec: Vector2, sprite: Sprite) -> Vector2:
	return vec * sprite_get_bounds(sprite)

# Takes a unit vector and returns a vector where both x and y value are exactly
# either 1 or 0
static func discretize_unit_vector(vec: Vector2):
	return (vec.normalized() + Vector2(.5,.5)).floor()

# Maps cardinal actions to one vector, useful for moving things around
# For the actions to match the ones you have in game, you should pass an array
# of strings representing the actions, clock-wise, starting from the top.
# it defaults to `['up','right','down','left']`
static func actions_to_vector(directions: PoolStringArray = ['up','right','down','left']) -> Vector2:
	return Vector2( \
		Input.get_action_strength(directions[1]) - \
		Input.get_action_strength(directions[3]) \
		, \
		Input.get_action_strength(directions[2]) - \
		Input.get_action_strength(directions[0]) \
	)

# Maps a Vector2 to the 4 cardinal directions, and dispatches an `action_press`
# which simulates an action press in Godot.
# For the actions to match the ones you have in game, you should pass an array
# of strings representing the actions, clock-wise, starting from the top.
# it defaults to `['up','right','down','left']`
# If you pass a ZERO vector, then all inputs will be released
static func direction_to_action(vec: Vector2, directions: PoolStringArray = ['up','right','down','left']):
	if vec == null or vec == Vector2.ZERO:
		for key in directions:
			Input.action_release(key)
		return

	if vec.x > 0:
		Input.action_press(directions[1],vec.x)
		Input.action_release(directions[3])
	elif vec.x < 0:
		Input.action_press(directions[3], - vec.x)
		Input.action_release(directions[1])
	else:
		Input.action_release(directions[1])
		Input.action_release(directions[3])
	if vec.y > 0:
		Input.action_press(directions[2], vec.y)
		Input.action_release(directions[0])
	elif vec.y < 0:
		Input.action_press(directions[0], - vec.y)
		Input.action_release(directions[2])
	else:
		Input.action_release(directions[2])
		Input.action_release(directions[0])

# Verifies if the even is `pressed` and if it is pressed over
# the given sprite.
static func sprite_is_pressed(event: InputEvent, sprite: Sprite) -> bool:
	return \
		'pressed' in event and \
		event.pressed == true and \
		sprite_has_position(event.position, sprite)

# Returns the sprite's bounds
static func sprite_get_bounds(sprite: Sprite) -> Vector2:
	return - sprite.offset + ((sprite.texture.get_size() / 2.0) if sprite.centered else Vector2.ZERO )

static func get_sprite_rect(sprite: Sprite) -> Rect2:
	var size = sprite.texture.get_size()
	var pos = sprite.global_position + sprite.offset - ((size / 2.0) if sprite.centered else Vector2.ZERO )
	var rect = Rect2(pos, size * sprite.scale)
	return rect

# verifies that the given position is enclosed in the sprite 
static func sprite_has_position(cursor_position: Vector2, sprite: Sprite) -> bool:
	var rect = get_sprite_rect(sprite)
	return rect.has_point(cursor_position)

# Returns a unit vector (a vector between 0 and 1) from the given cursor position
# and area size. The further away from the center the cursor is, the closer to 1
# the resulting vector will be
static func get_unit_vector(cursor_position:Vector2, area_size: Vector2, origin_is_center:bool = true, offset: Vector2 = Vector2.ZERO) -> Vector2:
	var character_pos = cursor_position + offset - (Vector2.ZERO if origin_is_center else area_size/2.0)
	var position = ((character_pos / area_size) * 2)
	var vec = Vector2(clamp(position.x, -1, 1), clamp(position.y, -1, 1))
	return vec

# Returns a unit vector (a vector between 0 and 1) from the given cursor position,
# an initial position, and area size. The further away from the initial position
# the cursor is, the closer to 1 the resulting vector will be
static func get_unit_vector_non_centered(cursor_position: Vector2, character_position: Vector2, area_size: Vector2):
	var pane = area_size - character_position
	var diff = (cursor_position - character_position)
	var position = (diff / pane)
	var vec = Vector2(clamp(position.x, -1, 1), clamp(position.y, -1, 1))
	return vec